<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://maris.beer/
 * @since      1.0.0
 *
 * @package    Eintopf_Calendar
 * @subpackage Eintopf_Calendar/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Eintopf_Calendar
 * @subpackage Eintopf_Calendar/includes
 * @author     Maris Beer <maris@stuttgartdorfuture.de>
 */
class Eintopf_Calendar_Deactivator
{

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate()
	{
		// remove cron job
		wp_clear_scheduled_hook('eintopf_calendar_updater');

		// remove table from database
		self::drop_table();
	}

	/**
	 * Drop custom table from database.
	 * 
	 * @since    1.0.0
	 */
	private static function drop_table()
	{
		global $wpdb;
		$table_name = $wpdb->prefix . str_replace('-', '_',  'eintopf-calendar') . '_data';
		$sql = "DROP TABLE IF EXISTS $table_name";
		$wpdb->query($sql);
	}
}
