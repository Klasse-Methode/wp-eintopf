<h1 align="center"> EINTOPF Wordpress Integration </h1> <br>
<p align="center">
  <a href="/Klasse-Methode/eintopf">
    <img alt="Eintopf Logo" title="EINTOPF" src="https://codeberg.org/repo-avatars/44037-abb329816f8899104114d17b9ab140f0" width="200">
  </a>
</p>

<p align="center">
  Der Kulturkalender - jetzt auch auf deiner Webseite
</p>

<p align="center">
  <!--<a href="#notyet">
  Install via Wordpress Plugin Directory
    <img alt="Install via Wordpress Plugin Directory" title="Wordpress Plugin Directory" src="https://s.w.org/style/images/about/WordPress-logotype-wmark.png" width="80">
  </a>-->
</p>

[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

## Development

You can use the [docker-compose file](docker-compose.yaml) to start a wordpress instance, with the current directory set up as a volume.

```
docker-compose up
```

Then open `http://localhost:8080` in your browser.

## Release

```
rm -rf wp-eintopf.zip && zip -x .git/**\* .gitignore .git/ .editorconfig docker-compose.yaml -r wp-eintopf.zip .
```
