# Copyright (C) 2022 Maris Beer
# This file is distributed under the GPL-2.0+.
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Eintopf Calendar Integration 1.0.0\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/eintopf-calendar\n"
"POT-Creation-Date: 2022-07-03 15:08+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.1\n"
"X-Domain: eintopf-calendar\n"

#. Plugin Name of the plugin
msgid "Eintopf Calendar Integration"
msgstr ""

#. Plugin URI of the plugin
msgid "https://eintopf.info/"
msgstr ""

#. Description of the plugin
msgid "This Plugin integrates the Eintopf Calendar into your WordPress site."
msgstr ""

#. Author of the plugin
msgid "Maris Beer"
msgstr ""

#. Author URI of the plugin
msgid "https://maris.beer/"
msgstr ""
