��          t      �                 "     ?  
   G     R     W  E   `     �     �     �  b  �     9     J  	   g  
   q  	   |     �  \   �     �     �                        
          	                               Eintopf Calendar Eintopf Calendar Integration General Maris Beer Save Settings This Plugin integrates the Eintopf Calendar into your WordPress site. Title https://eintopf.info/ https://maris.beer/ Project-Id-Version: eintopf-calendar
PO-Revision-Date: 2022-07-03 15:10+0200
Last-Translator: 
Language-Team: 
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: _e
X-Poedit-SearchPath-0: .
 Eintopf Kalender Eintopf Kalender Integration Allgemein Maris Beer Speichern Einstellungen Dieses Plugin ermöglicht es eine Eintopf Installation in deine Wordpress Seite einzubinden. Titel https://eintopf.info/ https://maris.beer/ 