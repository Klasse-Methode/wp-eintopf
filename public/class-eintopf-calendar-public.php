<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://maris.beer/
 * @since      1.0.0
 *
 * @package    Eintopf_Calendar
 * @subpackage Eintopf_Calendar/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Eintopf_Calendar
 * @subpackage Eintopf_Calendar/public
 * @author     Maris Beer <maris@stuttgartdorfuture.de>
 */
class Eintopf_Calendar_Public
{

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Maps numerical representation of a week day to text representation of the week day.
	 */
	private $dayMap = [ 'Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag' ];

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct($plugin_name, $version)
	{

		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles()
	{

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Eintopf_Calendar_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Eintopf_Calendar_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/eintopf-calendar-public.css', array(), $this->version, 'all');
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts()
	{

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Eintopf_Calendar_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Eintopf_Calendar_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/eintopf-calendar-public.js', array('jquery'), $this->version, false);
	}

	/**
	 * Register full shortcode
	 *
	 * @since    1.0.0
	 *
	 * @example [eintopf-calendar-list groupID=my-id title=my-title infos=time,location,organizers,involved,description link=true]
	 */
	public function eintopf_calendar_list_shortcode( $atts ) {
		$options = get_option('eintopf-calendar');

		$groupID = $atts['groupid'];
		$title = $atts['title'];
		$infos = $atts['infos'] ? explode(',', $atts['infos']) : ['time', 'place', 'organizers, involved', 'description'];
		function show_info($info, $infos) {
			return in_array($info, $infos);
		}

		$start = new DateTime();
		$startStr = str_replace('+', 'Z', $start->format(\DateTime::RFC3339));
		$startStr = str_replace('00:00', '', $startStr);
		$params = [
			'groupIDs' => $groupID,
			'sort' => 'start',
			'startMin' => $startStr
		];
		$events = $this->load_events($params);

		foreach ($events as $key => $event) {
			$events[$key] = $this->convertEvent($event);
			if ($atts['link'] === "true") {
				$events[$key]->link = $options['instance-url'] . '/event/' . $events[$key]->id;
			}
		}

		$eintopfURL = $this->buildURL('/', $params);

		ob_start();

		require_once plugin_dir_path(__FILE__) . 'partials/eintopf-calendar-list.php';

		return ob_get_clean();
	}

	/**
	 * Register full shortcode
	 *
	 * @since    1.0.0
	 *
	 * @example [eintopf-calendar-timetable id=my-id places="foo,bar" startHour=10 endHour=23]
	 */
	public function eintopf_calendar_timetable_shortcode( $atts ) {
		$options = get_option('eintopf-calendar');

		$startHour = intval($atts['starthour']);
		$endHour = intval($atts['endhour']);

		$places = explode(',', $atts['places']);
		$events = $this->load_events([ 'id' => $atts['id'], 'multidayMax' => 0 ]);

		if ($events == null) {
			return;
		}
		if (count($events) === 0) {
			return;
		}
		$events = $events[0]->children;

		$days = [];

		$currentDay = new DateTime();
		$currentDay = $currentDay->format('Y-m-d');
		$foundCurrentDay = false;

		foreach($events as $rawEvent) {
			$start = new DateTime($rawEvent->start);
			$end = new DateTime($rawEvent->end);

			// Create a new day, if it doesn't exit yet.
			$day = $start->format('Y-m-d');
			if (!array_key_exists($day, $days)) {
				$startDay = new DateTime($day);
				$startDay->setTime($startHour, 00);

				$endDay = new DateTime($day);
				$endDay->setTime($endHour, 00);

				// Check if the $day is the current day.
				$isCurrentDay = $currentDay == $day;
				if ($isCurrentDay) {
					$foundCurrentDay = true;
				}

				$days[$day] = [
					'title' => $this->dayMap[$start->format('w')] . $start->format(' d.n'),
					'height' => $this->timetableYOffset($endDay, $startHour) - $this->timetableYOffset($startDay, $startHour),
					'start' => $startDay,
					'end' => $endDay,
					'active' => $isCurrentDay,
					'events' => []
				];
			}

			$event = $this->convertEvent($rawEvent);
			// col is set to "all", if the place was not found in the $places array.
			$col = array_search($event->location, $places);
			$event->col = $col === false ? 'all' : $col + 1;
			$event->top = $this->timetableYOffset($start, $startHour);
			$event->height = $this->timetableYOffset($end, $startHour) - $event->top;

			$days[$day]['events'][] = $event;
		}


		// Set the first day as active if none of the days is the current one.
		if (!$foundCurrentDay) {
			$days[array_key_first($days)]['active'] = true;
		}

		ob_start();

		require_once plugin_dir_path(__FILE__) . 'partials/eintopf-calendar-timetable.php';

		return ob_get_clean();
	}

	private function convertEvent($rawEvent) {
		$event = new stdClass();
		$event->id = $rawEvent->id;
		$event->canceled = $rawEvent->canceled;
		$event->name = $rawEvent->name;
		if (strlen($rawEvent->location2) > 0) {
			$event->location = $rawEvent->location2;
		// } else if (strlen($rawEvent->location->name) > 0) {
		// 	$event->location = $rawEvent->location->name;
		// } else if ($rawEvent->location->place !== null) {
		// 	$event->location = $rawEvent->location->place->name;
		}
		$event->organizers = [];

		foreach ($rawEvent->organizers as $organizer) {
			if (strlen($organizer->name) > 0) {
				// add organizer to array
				$event->organizers[] = $organizer->name;
			} else if ($organizer->group !== null) {
				$event->organizers[] = $organizer->group->name;
			}
		}
		$event->involved = $rawEvent->involved;
		$event->description = $rawEvent->description;
		$event->tags = $rawEvent->tags;
		$event->start = new DateTime($rawEvent->start);
		$event->end = $rawEvent->end !== "" ? new DateTime($rawEvent->end) : null;
		return $event;
	}

	/**
	 * Calculates the y offset in the timetable.
	 *
	 * @param DateTime $time      The time to calculate the y offset for.
	 * @param number   $startHour The starting hour of the timetable.
	 *
	 * @return number
	 */
	private function timetableYOffset($time, $startHour) {
		$hour = intval($time->format('H')) - $startHour;
		$minutes = intval($time->format('i'));
		return intval(($hour * 60 + $minutes));
	}

	/**
	 * Loads all sub events for a given event id.
	 *
	 * @param array $params Eventsearch parameters
	 *
	 * @return array|null
	 */
	private function load_events($params) {
		$url = $this->buildURL('/api/v1/eventsearch', $params);
		try {
			$response = wp_remote_get($url, [ 'timeout' => 60 ]);
			$response_code = wp_remote_retrieve_response_code($response);
			if ($response_code == 200) {
				$response_body = wp_remote_retrieve_body($response);
				$response_body = json_decode($response_body);
			} else {
				var_dump($url);
				var_dump($response);
				var_dump($response_body->events); exit;
				return null;
			}
		} catch (Exception $e) {
			return null;
		}
		return $response_body->events;
	}

	private function buildURL($uri, $params) {
		$options = get_option('eintopf-calendar');

		if (empty($options['instance-url'])) {
			return null;
		}
		return $options['instance-url'] . $uri . '?' . http_build_query($params);
	}
}
