<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://maris.beer/
 * @since      1.0.0
 *
 * @package    Eintopf_Calendar
 * @subpackage Eintopf_Calendar/public/partials
 */

?>
<div class="eintopf-container">
	<div class="eintopf-header">
		<?= $title ?? "Veranstaltungen" ?>
	</div>
	<div class="eintopf-list">
		<?php if ($events === null || count($events) === 0) : ?>
			Aktuell sind keine Veranstaltungen geplant
		<?php endif; ?>
		<?php foreach($events as $event) : ?>
			<?php if ($event->link) : ?><a href="<?php echo $event->link;?>" target="blank"<?php else: ?><div<?php endif; ?>
				id="<?php echo $event->id; ?>"
					class="h-event eintopf-event eintopf-cols<?php if (!$event->link): ?> canOpen<?php endif; ?><?php if ($event->canceled): ?> canceled<?php endif; ?>"
				itemscope itemtype="https://schema.org/Event"
			>
				<div class="eintopf-left">
					<?php echo wp_date('d.m', $event->start->getTimestamp()); ?>
				</div>
				<div class="eintopf-right">
					<h2 class="p-name" itemprop="name"><?php if ($event->canceled): ?>FÄLLT AUS: <?php endif; ?><?php echo $event->name; ?></h2>
					<?php if (show_info('time', $infos)) : ?>
						<span class="eintopf-info eintopf-time">
							Wann:
							<time class="dt-start" itemprop="startDate" datetime="<?php echo $event->start->format(DateTime::ATOM); ?>">
								<?php echo $event->start->format('H:i'); ?>
							</time>

							<?php if ($event->end != null) : ?>
								-
								<time class="dt-end" itemprop="endDate" datetime="<?php echo $event->end->format(DateTime::ATOM); ?>">
									<?php echo $event->end->format('H:i'); ?>
								</time>
								Uhr
							<?php endif; ?>
						</span>
					<?php endif ?>

					<?php if (show_info('location', $infos) && $event->location) : ?>
						<div class="eintopf-info eintopf-location">
							Wo: <span class="p-location" itemprop="location"><?php echo $event->location ?></span>
						</div>
					<?php endif ?>

					<?php if (show_info('organizers', $infos) && count($event->organizers) > 0) : ?>
						<div class="eintopf-info eintopf-organizers">

							<div>Von:
								<?php foreach($event->organizers as $o => $organizer) : ?>
									<span itemprop="organizer" ><?php echo $organizer ?><?php if ($o+1 < count($event->organizers)) : ?>, <?php endif ?></span>
								<?php endforeach ?>
							</div>
						</div>
					<?php endif ?>

					<?php if (show_info('involved', $infos) && count($event->involved) > 0) : ?>
						<div class="eintopf-info eintopf-involved">
							<div>Mit:
								<?php foreach($event->involved as $i => $involved) : ?>
									<span itemprop="contributor"><?php echo $involved->name ?></span><?php if ($i+1 < count($event->involved)) : ?>, <?php endif ?>
								<?php endforeach ?>
							</div>
						</div>
					<?php endif ?>

					<?php if (show_info('description', $infos)) : ?>
						<div class="p-summary eintopf-info eintopf-description eintopf-more" itemprop="description">
							<br />
							<?php echo str_replace('\n', '<br />', $event->description) ?>
						</div>
					<?php endif ?>
				</div>
			<?php if ($event->link) : ?></a><?php else: ?></div><?php endif; ?>
		<?php endforeach ?>
	</div>
	<div class="eintopf-powered-by">powered by <a href="<?php echo $eintopfURL; ?>">EINTOPF</a></div>
</div>
