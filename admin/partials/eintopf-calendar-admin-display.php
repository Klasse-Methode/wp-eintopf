<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://maris.beer/
 * @since      1.0.0
 *
 * @package    Eintopf_Calendar
 * @subpackage Eintopf_Calendar/admin/partials
 */

?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<!-- mdb centered form -->
<div class="row">
    <form method="post" action="options.php">
        <div class="col-md-4">
            <div class="card">
                <?php
                settings_fields($this->plugin_name);
                ?>
                <div class="card-header"><?php _e('General', $this->plugin_name); ?></div>
                <div class="card-body">
                    <div class="card-text">
                        <div class="form-outline" style="margin-top: 20px;">
                            <input type="url" id="<?php echo $this->plugin_name ?>[instance-url]" name="<?php echo $this->plugin_name ?>[instance-url]" class="form-control" value="<?php echo esc_attr(get_option($this->plugin_name)['instance-url'] ?? ''); ?>" />
                            <label class="form-label" for="<?php $this->plugin_name ?>[instance-url]"><?php _e('Instance URL', $this->plugin_name); ?></label>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <!-- mdb submit button aligned to the right -->
                    <button type="submit" class="btn btn-primary btn-block"><?php _e('Save', $this->plugin_name); ?></button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- /mdb centered form -->
