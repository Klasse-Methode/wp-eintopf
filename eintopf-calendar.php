<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://maris.beer/
 * @since             1.0.0
 * @package           Eintopf_Calendar
 *
 * @wordpress-plugin
 * Plugin Name:       Eintopf Calendar Integration
 * Plugin URI:        https://eintopf.info/
 * Description:       This Plugin integrates the Eintopf Calendar into your WordPress site.
 * Version:           1.0.0
 * Author:            Maris Beer
 * Author URI:        https://maris.beer/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       eintopf-calendar
 * Domain Path:       /languages/
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'EINTOPF_CALENDAR_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-eintopf-calendar-activator.php
 */
function activate_eintopf_calendar() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-eintopf-calendar-activator.php';
	Eintopf_Calendar_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-eintopf-calendar-deactivator.php
 */
function deactivate_eintopf_calendar() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-eintopf-calendar-deactivator.php';
	Eintopf_Calendar_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_eintopf_calendar' );
register_deactivation_hook( __FILE__, 'deactivate_eintopf_calendar' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-eintopf-calendar.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_eintopf_calendar() {

	$plugin = new Eintopf_Calendar();
	$plugin->run();

}
run_eintopf_calendar();
